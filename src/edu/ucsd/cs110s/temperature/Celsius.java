/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110saf
 *
 */
public class Celsius extends Temperature {
	
	public Celsius(float t) 
	 { 
	 super(t);  
	 } 
	
	 public String toString() 
	 { 
		 return "" + this.getValue() + ""; 
	 }
	 
	@Override
	public Temperature toCelsius () {
		float value = (this.getValue ());
		Temperature ret = new Celsius (value);
		
		return ret;
	}
	
	@Override
	public Temperature toFahrenheit() {
		float value =  (float) (1.8 * this.getValue() + 32);
		Temperature ret = new Fahrenheit (value);
		
		return ret;
	}

}
