/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110saf
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) 
	 { 
	 	super(t); 
	 } 
	 public String toString() 
	 { 
		 return  "" + this.getValue() + ""; 
	 }
	 
	@Override
	public Temperature toCelsius() {
		float value = (float) ((this.getValue() - 32) * 5/9);
		Temperature ret = new Celsius (value);
		
		return ret;
	}
	
	@Override
	public Temperature toFahrenheit() {
		float value =  (this.getValue ());;
		Temperature ret = new Fahrenheit (value);
		
		return ret;
	} 


}
